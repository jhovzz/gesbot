const dotenv = require("dotenv").config();
const app = require('./src/lib/slack-app')
const slackController = require("./src/lib/slack/slack-events-controller");


app.event("app_mention", slackController.appMentioned);

(async () => {
  await app.start();
  console.log("⚡️ Bolt app is running!");
})();
