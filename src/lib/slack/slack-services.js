const defaultReplyToMention = async (username) => {
  return `Yes <@${username}>?`
}

const getProjectResponse = async (event, commands) => {
  console.log(`get all ${commands[0]}`)
  return `<@${event.user}> This will show all current projects`
}

const getIssuesResponse = async (event, commands) => {
  console.log(`get all ${commands[0]}`)
  return `<@${event.user}> This will show all current issues`
}



module.exports = {
  defaultReplyToMention,
  getProjectResponse,
  getIssuesResponse
}