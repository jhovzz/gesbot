const { defaultReplyToMention,  getIssuesResponse, getProjectResponse} = require("./slack-services");

const appMentioned = async ({ event, say }) => {
  const commands = event.text.split(" ");
  commands.shift();

  switch (commands[0]) {
    case "update":
      console.log("updates response will go here!!");
      break;

    case "projects":
      await say(await getProjectResponse(event, commands));
      break;

    case "issues":
      await say(await getIssuesResponse(event, commands))
      break;

    default:
      await say(await defaultReplyToMention(event.user));
      break;
  }
};

module.exports = {
  appMentioned,
};
